import 'dart:io';

void main() {
  var pikachu = Pokemon();
  print(pikachu.name);
  pikachu.power();
}

class Game {
  String name = 'Pikachu';

  void power() {
    print('Pokemon power is...');
  }
}

class Pokemon extends Game {
  String gender = "";

  @override
  String name = "";

  @override
  void power() {
    super.power();
    print('Pokemon power is Electric');
    print('Name : ' + super.name);
  }
}
